#!/usr/bin/env bash
environment="local"
mysqlhost="127.0.0.1"
mysqluser="root"
mysqlpassword="vagrant"
developbranch="develop"
rootfolder="webroot"
nginxversion="1.12.2"
phpversion="7.3"
nodeversion="8.10.0"
bbmessage="Copy your ssh-key to Bitbucket"

sudo add-apt-repository ppa:ondrej/php
sudo apt-get update
sudo apt-get install -y python-software-properties unzip

echo -e "\e[31m=====INSTALL GIT=====\e[0m"
sudo apt-get install -y git-core

echo -e "\e[31m=====INSTALLING NGINX=====\e[0m"
sudo apt-get install -y nginx

echo -e "\e[31m=====OVERRIDE DEFAULT NGINX CONF=====\e[0m"

sudo cp /vagrant/app_php$phpversion.conf /etc/nginx/sites-available/app.conf

sudo ln -s /etc/nginx/sites-available/app.conf /etc/nginx/sites-enabled/app.conf
sudo service nginx restart

if ! [ -L /var/www ]; then
    sudo  rm -rf /var/www
	sudo ln -fs /vagrant /var/www
fi

echo -e "\e[31m=====INSTALLING POSTGRES=====\e[0m"
sudo apt-get install -y postgresql postgresql-contrib
sudo -u postgres createuser acquire -s
sudo -u postgres psql -c "ALTER USER acquire WITH ENCRYPTED PASSWORD 'acquire'"
sudo -u postgres createdb acquire

echo -e "\e[31m=====INSTALLING PHP $phpversion AND MODULES=====\e[0m"
sudo apt-get install -y php$phpversion-dev php$phpversion-cli php$phpversion-common php$phpversion php$phpversion-pgsql php$phpversion-fpm php$phpversion-curl php$phpversion-gd php$phpversion-xml php$phpversion-zip php$phpversion-mbstring php-memcached ntpdate

echo -e "\e[31m===== INSTALLING MCRYPT $phpversion =====\e[0m"
if [ $phpversion = "7.1" ]; then
    sudo apt-get install -y php$phpversion-mcrypt
else
    sudo apt-get -y install gcc make autoconf libc-dev pkg-config
    sudo apt-get -y install libmcrypt-dev
    pecl channel-update pecl.php.net
    sudo pecl install mcrypt-1.0.2
    sudo bash -c "echo extension=/usr/lib/php/20180731/mcrypt.so > /etc/php/7.3/cli/conf.d/mcrypt.ini"
fi

echo -e "\e[31m=====SYNC SERVER TIME=====\e[0m"
sudo ntpdate time.apple.com

echo -e "\e[31m=====CONFIGURE SERVER TIMEZONE DATA=====\e[0m"
sudo dpkg-reconfigure tzdata

echo -e "\e[31m=====INSTALLING COMPOSER=====\e[0m"
cd /vagrant
php -r "readfile('https://getcomposer.org/installer');" | php
sudo mv composer.phar /usr/local/bin/composer
sudo chmod 777 /usr/local/bin/composer

echo -e "\e[31m=====INSTALL NODE=====\e[0m"
cd /vagrant
wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.33.8/install.sh | bash
source /home/vagrant/.nvm/nvm.sh
echo "source ~/.nvm/nvm.sh" >> ~/.bashrc
nvm install v8.10.0

echo -e "\e[31m=====SETUP SWAP FILE=====\e[0m"
sudo /bin/dd if=/dev/zero of=/var/swap.1 bs=1M count=1024
sudo /sbin/mkswap /var/swap.1
sudo /sbin/swapon /var/swap.1

echo -e "\e[31m=====INSTALL REDIS=====\e[0m"
sudo apt-get install -y redis-server redis-sentinel redis-tools

echo -e "\e[31m=====START REDIS AS SERVICE=====\e[0m"
sudo service redis-server start

echo -e "\e[31m=====SET REDIS TO AUTOSTART=====\e[0m"
sudo update-rc.d redis-server enable
sudo update-rc.d redis-server defaults

echo -e "\e[31m=====SET REDIS TO AUTOSTART=====\e[0m"

echo -e "\e[31m=====CREATE MYSQL TIMEZONE TABLES=====\e[0m"
mysql_tzinfo_to_sql /usr/share/zoneinfo | mysql -u $mysqluser -p$mysqlpassword mysql

echo -e "\e[31m=====START NGINX=====\e[0m"
sudo service nginx start

echo -e "\e[31m=====RESTART PHP-FPM=====\e[0m"
sudo service php$phpversion-fpm restart

echo -e "\e[31m=====SET SUPERVISOR SERVICE FOR LARAVEL JOBS=====\e[0m"
sudo apt-get install -y supervisor
sudo cp /vagrant/worker.conf /etc/supervisor/conf.d/
sudo supervisorctl reread
sudo supervisorctl update
sudo supervisorctl start all

echo -e "\e[31m=====SET CRON JOB=====\e[0m"
sudo cp /vagrant/app /etc/cron.d
sudo service cron restart
sudo service cron reload

echo -e "\e[31m=====CLEARING APP CACHES=====\e[0m"
cd /vagrant/webroot
composer install
php artisan config:clear
php artisan cache:clear
php artisan clear-compiled
composer dump-autoload
php artisan config:cache

npm install
php artisan migrate
php artisan db:seed --class=AdminUser
php artisan acquire:fill-model-table
npm run dev

echo -e "\e[31m=====VAGRANT PROVISIONING DONE=====\e[0m"
