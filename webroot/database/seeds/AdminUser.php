<?php

use Illuminate\Database\Seeder;

class AdminUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $user = \App\Models\User::firstOrCreate(['name' => 'Admin', 'email' => 'admin@acquire.com'], [
            'password' => bcrypt('password'),
            'is_superuser' => true
        ]);

    }
}
