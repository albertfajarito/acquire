<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Auth\LoginController@showLoginForm');
Route::get('/login', 'Auth\LoginController@showLoginForm');

Route::post('/login', 'Auth\LoginController@login')->name('login');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

Route::group(['middleware' => 'auth'], function(){
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    Route::get('/role/create', 'RoleController@showForm');
    Route::post('/role/create', 'RoleController@create');
    Route::get('/role/edit/{id}', 'RoleController@showForm');
    Route::post('/role/edit', 'RoleController@update');
    Route::get('/role/list', 'RoleController@view');
    Route::get('/role/permission/{role_id}', 'RolePermissionController@view');
    Route::post('/role/permission/save', 'RolePermissionController@save');
    Route::get('/user/create', 'UserController@showForm');
    Route::post('/user/create', 'UserController@create');
    Route::get('/user/view/{id?}', 'UserController@view');
    Route::get('/user/export/{type}', 'UserController@export');
    Route::get('/user/edit/{id}', 'UserController@showForm');
    Route::post('/user/edit/{id}', 'UserController@update');
    Route::get('/user/change-password/{id}', 'UserController@showForm');
    Route::post('/user/change-password', 'UserController@changePassword');
    Route::get('/dashboard/reports/{user_id}', 'MyReportController@show');
});
