@extends('layouts.app')

<div id="app">
@section('content')

    @include('menu.top')

        <div class="container-fluid">
            <div class="row">
                @include('menu.left')

                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
                    @section('content-window')
                    @show
                </main>
            </div>
        </div>

@endsection
</div>
