@extends('layouts.app')

@section('top-menu')
    @include('menu.top')
@endsection

@section('content')
        @include('main.default')
@endsection
