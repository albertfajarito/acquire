    @extends('layouts.dashboard')

    @section('content-window')
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">{{ __('Admin') }}</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
            </div>
        </div>

        <h2>
            @switch($context)
                @case('create')
                {{ __('Create User') }}
                @break

                @case('view')
                {{ __('View User') }}
                @break

                @case('edit')
                {{ __('Edit User') }}
                @break

                @case('change-password')
                {{ __('Change User Password') }}
                @break
            @endswitch
        </h2>

        @if (session()->has('status'))
            @php $message = '' @endphp
            <div class="alert alert-{{ session()->get('status') }}" role="alert">
                @if (session('status') == 'success' && session()->has('context'))
                    @if (session('context') == 'create')
                        @php $message = 'User created successfully' @endphp
                    @elseif (session('context') == 'change-password')
                        @php $message = 'User password successfully updated' @endphp
                    @endif
                @else
                    @if (session('context') == 'create')
                        @php $message = 'Failed to create user' @endphp
                    @elseif (session('context') == 'change-password')
                        @php $message = 'Failed to update user password' @endphp
                    @endif
                @endif
                <strong>{{ $message }}</strong>
            </div>
        @endif

        <div class="">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                        @switch($context)
                            @case('create')
                                {{ __('New User') }}
                            @break

                            @case('view')
                                {{ __('User #'.$user->id) }}
                            @break

                            @case('edit')
                                {{ __('User #'.$user->id) }}
                            @break

                            @case('change-password')
                                {{ __($user->name." ".$user->second_name." (".$user->email).")" }}
                            @break

                        @endswitch
                        </div>

                        <form class="mb-0" method="POST"
                            @switch($context)
                                @case('create')
                                action="{{ url('/user/create') }}"
                                @break

                                @case('edit')
                                action="{{ url('/user/edit/'.$user->id) }}"
                                @break

                                @case('change-password')
                                action="{{ url('/user/change-password') }}"
                                @break
                            @endswitch
                        >
                        <div class="card-body">

                                @csrf

                            @if($context != 'change-password')
                                <div class="form-group row">
                                    <label for="name" class="col-md-2 col-form-label text-md-right">{{ __('First Name*') }}</label>

                                    <div class="col-md-10">
                                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}{{ $user->name ?? ''  }}" required autofocus @exists @if($context=='view'){{ 'disabled'}}@endif >

                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="second_name" class="col-md-2 col-form-label text-md-right">{{ __('Last Name*') }}</label>

                                    <div class="col-md-10">
                                        <input id="second_name" type="text" class="form-control{{ $errors->has('second_name') ? ' is-invalid' : '' }}" name="second_name" value="{{ old('second_name') }}{{ $user->second_name ?? ''  }}" required autofocus  @if($context=='view'){{ 'disabled'}}@endif>

                                        @if ($errors->has('second_name'))
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('second_name') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="email" class="col-md-2 col-form-label text-md-right">{{ __('Email*') }}</label>

                                    <div class="col-md-10">
                                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}{{ $user->email ?? ''  }}" required autofocus @if($context=='view'){{ 'disabled'}}@endif >

                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            @endif
                            @if($context == 'change-password')
                                <div class="form-group row">
                                    <label for="current_password" class="col-md-2 col-form-label text-md-right">{{ __('Current Password*') }}</label>

                                    <div class="col-md-10">
                                        <input id="current_password" type="password" class="form-control{{ $errors->has('current_password') ? ' is-invalid' : '' }}" name="current_password" value="" required autofocus>

                                        @if ($errors->has('current_password'))
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('current_password') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            @endif

                            @if($context == 'create' || $context == 'change-password')
                            <div class="form-group row">
                                <label for="password" class="col-md-2 col-form-label text-md-right">{{ $context == 'change-password'? 'New ' : '' }}{{ __('Password*') }}</label>

                                <div class="col-md-10">
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" value="" required autofocus>

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="confirm_password" class="col-md-2 col-form-label text-md-right">{{ $context == 'change-password'?  __('Confirm New Password*') : __('Confirm Password*') }}</label>

                                <div class="col-md-10">
                                    <input id="confirm_password" type="password" class="form-control{{ $errors->has('confirm_password') ? ' is-invalid' : '' }}" name="confirm_password" value="" required autofocus>

                                    @if ($errors->has('confirm_password'))
                                        <span c
                                              lass="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('confirm_password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            @endif
                            @if($context != 'change-password')
                                <div class="form-group row">
                                    <label for="user_roles" class="col-md-2 col-form-label text-md-right">{{ __('User Role(s)*') }}</label>

                                    <div class="col-md-10">
                                        @if($context == 'edit')
                                            <select id="user_roles" name="user_roles[]" class="custom-select {{ $errors->has('user_roles') ? ' is-invalid' : '' }}" multiple @if($context=='view'){{'disabled'}}@endisset>
                                                @foreach( $roles AS $role )
                                                    <option value="{{ $role->id }}"
                                                    @foreach($user->userroles AS $userrole) @if($userrole->role->id == $role->id){{ 'selected' }}@endif @endforeach>
                                                        {{ $role->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        @elseif($context == 'create')
                                            <select id="user_roles" name="user_roles[]" class="custom-select {{ $errors->has('user_roles') ? ' is-invalid' : '' }}" multiple @if($context=='view'){{'disabled'}}@endisset>
                                                @foreach( $roles AS $role )
                                                    <option value="{{ $role->id }}">{{ $role->name }}</option>
                                                @endforeach
                                            </select>
                                        @elseif($context == 'view')
                                            <ul class="list">
                                                @foreach($user->userroles AS $userrole)
                                                    <li class="item">{{ $userrole->role->name }}</li>
                                                @endforeach
                                            </ul>
                                        @endif
                                        @if ($errors->has('user_roles'))
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('user_roles') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            @endif
                        </div>
                            <div class="card-footer">

                                <div class="form-group row mb-0">


                                    <div class="col-md-6 offset-md-6">
                                        @if($context != 'view' )
                                        <button type="submit" class="btn btn-primary float-right">
                                            @if($context == 'create')
                                                {{ __('Create') }}
                                            @else
                                                {{ __('Update') }}
                                            @endif
                                        </button>
                                        @else
                                            <a href="{{ url('/user/edit/'.$user->id)  }}" class="btn btn-primary float-right">
                                                {{ __('Edit') }}
                                            </a>
                                            <a href="{{ url('/user/change-password/'.$user->id)  }}" class="btn btn-primary float-right mr-2">
                                                {{ __('Change Password') }}
                                            </a>
                                        @endif
                                    </div>


                                </div>
                            </div>
                            @if(in_array($context, ['change-password', 'edit']))
                        <input type="hidden" name="user_id" value="{{ $user->id }}">
                                @endif
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endsection


