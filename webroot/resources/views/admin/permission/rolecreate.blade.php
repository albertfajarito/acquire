@extends('layouts.dashboard')

@section('content-window')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">{{ __('Admin') }}</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
        </div>
    </div>

    <h2>{{ __('Create Role') }}</h2>

    @if (session()->has('status'))
        <div class="alert alert-{{ session()->get('status') }}" role="alert">
            @if (session()->has('status') == 'success')
                @php $message = 'Role created successfully' @endphp
            @else
                @php $message = 'Failed to add role' @endphp
            @endif
            <strong>{{ $message }}</strong>
        </div>
    @endif

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        @switch($context)
                            @case('create')
                                {{ __('New Role') }}
                            @break
                            @case('edit')
                                {{ __('Edit Role') }}
                            @break
                        @endswitch
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ url('/role/'.$context) }}">
                            @csrf

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Role Name*') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}{{ $role->name ?? ''  }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="isAdmin" class="col-md-4 col-form-label text-md-right">{{ __('Tag as Admin*') }}</label>

                                <div class="col-md-6">
                                    <input id="isAdmin" type="checkbox" name="isAdmin" required autofocus {{ old('isAdmin')? 'checked' : '' }} @if($context == 'view' || $context == 'edit'){{ $role->isAdmin? 'checked' : '' }}@endif>

                                    @if ($errors->has('isAdmin'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('isAdmin') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        @if($context == 'create')
                                            {{ __('Create') }}
                                        @else
                                            {{ __('Update') }}
                                        @endif
                                    </button>
                                </div>
                            </div>

                            @if($context != 'create')
                                <input type="hidden" name="role_id" value="{{ $role->id }}">
                            @endif
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


