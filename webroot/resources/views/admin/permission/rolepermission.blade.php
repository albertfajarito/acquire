@extends('layouts.dashboard')

@section('content-window')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">{{ __('Admin') }}</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
        </div>
    </div>

    <h2>{{ __(ucwords($role->name).' Permissions') }}</h2>

    @if (session()->has('status'))
        <div class="alert alert-{{ session()->get('status') }}" role="alert">
            @if (session()->has('status') == 'success')
                @php $message = 'Permission saved successfully' @endphp
            @else
                @php $message = 'Failed to save permissions' @endphp
            @endif
            <strong>{{ $message }}</strong>
        </div>
    @endif

        <div class="row tp-2">
            <div class="col-md-12">
                <div class="card">
                    <form class="mb-0" method="POST" action="{{ url('/role/permission/save') }}">
                    <div class="card-body">

                            @csrf

                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th></th>
                                    @foreach( config('permission') AS $name=>$value )
                                        <th>{{ ucwords($name) }}</th>
                                    @endforeach
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($models AS $model)
                                <tr>
                                    <td>{{ ucwords($model->name) }}</td>
                                    @foreach( config('permission') AS $name=>$value )
                                        <th><input type="checkbox" name="{{ strtolower($model->name)."[".$name."]" }}"
                                            value="{{ config('permission.'.$name.".bit")  }}"
                                            @if ($permission_data->where('name', strtolower($model->name))->count() > 0 && $permission_data->where('name', strtolower($model->name))->first()->get($name)->canDo())
                                                {{ 'checked' }}
                                            @endif
                                            >
                                        </th>
                                    @endforeach
                                </tr>
                                @endforeach
                                </tbody>
                            </table>



                    </div>
                    <div class="card-footer">
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-6">
                                <button type="submit" class="btn btn-primary float-right">
                                    {{ __('Save') }}
                                </button>
                            </div>
                        </div>
                        <input name="role_id" type="hidden" value="{{ $role->id }}">

                    </div>
                    </form>
            </div>
        </div>
@endsection


