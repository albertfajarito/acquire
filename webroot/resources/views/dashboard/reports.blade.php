@extends('layouts.dashboard')

@section('content-window')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">{{ __('Dashboard') }}</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
        </div>
    </div>

    <h2>{{ __('My Reports') }}</h2>


    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead class="thead-dark">
                    <tr>
                        <th>#</th>
                        <th>Report</th>
                        <th>Request Date</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                @foreach($reports as $report)
                    <tr>
                        <td>{{ $report->id }}</td>
                        <td>{{ $report->type }}</td>
                        <td>{{ date('Y-m-d', strtotime($report->created_at)) }}</td>
                        <td>{{ $report->status }}</td>
                        <td>
                            <a class="btn btn-sm">{{__('Download')}}</a>
                            <a class="btn btn-sm">{{__('Delete')}}</a>
                        </td>
                    </tr>
                @endforeach
            </table>
            {{ $reports->links() }}
        </div>
    </div>
@endsection


