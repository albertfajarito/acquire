@extends('layouts.dashboard')

@section('content-window')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">{{ __('Admin') }}</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
                <button class="btn btn-sm btn-outline-secondary disabled">Export:</button>
                <button class="btn btn-sm btn-outline-secondary">CSV</button>
                <button class="btn btn-sm btn-outline-secondary">Excel</button>
            </div>
        </div>
    </div>

    <h2>{{ __('User List') }}</h2>

    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Roles</th>
                    <th>Date Created</th>
                    <th></th>
                </tr>
                @foreach($users as $user)
                    <tr>
                        <td>{{ $user->id }}   </td>
                        <td>
                            @if(Auth::user()->is_superuser || !$user->is_superuser)
                                <a href="{{ url('/user/view/'.$user->id) }}">{{ $user->name." ".$user->second_name }}</a>
                            @else
                                {{ $user->name." ".$user->second_name }}
                            @endif
                        </td>
                        <td>{{ $user->email }}</td>
                        <td>
                            @if($user->is_superuser)
                                <button type="button" class="btn btn-sm btn-outline-success">{{ __('Super User') }}</button>
                            @endif
                            @foreach($user->userroles AS $userrole)
                                <button type="button" class="btn btn-sm btn-outline-secondary">{{ $userrole->role->name }}</button>
                            @endforeach
                        </td>
                        <td>{{ date('Y-m-d', strtotime($user->created_at)) }}</td>
                    </tr>
                @endforeach
                </thead>
            </table>
            {{ $users->links() }}
        </div>
    </div>
@endsection


