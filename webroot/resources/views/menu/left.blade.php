<nav class="col-md-2 d-none d-md-block bg-light sidebar">
    <div class="sidebar-sticky">
        <h6 class="align-items-center px-3 mt-4 mb-1 text-info">
            <i class="fas fa-home"></i>
            Dashboard
        </h6>
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link text-muted" href="#">
                    My Profile
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-muted" href="{{ url('/dashboard/reports/'.Auth::user()->id) }}">
                    My Reports
                </a>
            </li>
        </ul>

        @can('is-admin')
            <h6 class="align-items-center px-3 mt-4 mb-1 text-info">
                <i class="fas fa-crown"></i>
                Admin
            </h6>
            <ul class="nav flex-column">
                @canany(['add-user','view-user'])
                    <li class="nav-item text-dark">
                        <a class="nav-link" href="#">
                            <i class="fas fa-users text-muted"></i>
                            User Manager
                        </a>
                        <ul class="pl-4 nav flex-column">
                            @can('add-user', auth()->user())
                                <li class="nav-item">
                                    <a class="nav-link text-muted" href="{{ url('/user/create') }}">
                                        Create User
                                    </a>
                                </li>
                            @endcan
                            @can('view-user', auth()->user())
                                <li class="nav-item">
                                    <a class="nav-link text-muted" href="{{ url('/user/view') }}">
                                        User List
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan
            </ul>
            <ul class="nav flex-column">
                <li class="nav-item text-dark">
                    <a class="nav-link" href="#">
                        <i class="fas fa-shield-alt text-muted"></i>
                        Access
                    </a>
                    <ul class="pl-4 nav flex-column">
                        @can('add-role')
                            <li class="nav-item">
                                <a class="nav-link text-muted" href="{{ url('role/create') }}">
                                    Create Role
                                </a>
                            </li>
                        @endcan
                        @can('view-role', auth()->user())
                            <li class="nav-item">
                                <a class="nav-link text-muted" href="{{ url('role/list') }}">
                                    Role lists
                                </a>
                            </li>
                        @endcan
                    </ul>
                </li>
            </ul>
        @endcan
    </div>
</nav>
