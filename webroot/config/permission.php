<?php

return [
    'add' => [
        'bit' => 1
    ],
    'edit' => [
      'bit' => 2
    ],
    'delete' => [
      'bit' => 4
    ],
    'view' => [
      'bit' => 8
    ],
];
