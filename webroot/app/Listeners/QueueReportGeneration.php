<?php

namespace App\Listeners;

use App\Events\ReportRequested;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class QueueReportGeneration implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ReportRequested  $event
     * @return void
     */
    public function handle(ReportRequested $event)
    {
        //
    }
}
