<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model AS LaraModel;

class Model extends LaraModel
{
    //
    protected $fillable = [
        'name', 'class'
    ];
}
