<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoleModelPermission extends Model
{
    //
    protected $fillable = [
      'role_id',
      'model_id',
      'permission'
    ];

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function model()
    {
        return $this->belongsTo(\App\Models\Model::class);
    }
}
