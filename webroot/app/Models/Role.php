<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //
    protected $fillable = [
        'name',
        'isAdmin'
    ];

    public function rolemodelpermission()
    {
        return $this->hasMany(RoleModelPermission::class);
    }

    public function userrole()
    {
        return $this->hasOne(UserRole::class, 'id');
    }
}
