<?php

namespace App\Http\Controllers;

use App\Events\ReportRequested;
use App\Http\Requests\ChangeUserPassword;
use App\Models\Model;
use App\Models\ReportRequest;
use App\Models\Role;
use App\Models\User;
use App\Models\UserRole;
use App\Traits\ContextualView;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Nexmo\Call\Event;
use SebastianBergmann\CodeCoverage\Report\Xml\Report;

class UserController extends Controller
{
    use ContextualView;

    public function create(\App\Http\Requests\User $request)
    {
        $user_roles = $request->has('user_roles')? $request->get('user_roles') : [];

        $user = User::updateOrCreate(['email' => $request->email], array_merge($request->toArray(), [
            'password' => bcrypt($request->get('password'))]
        ));

        UserRole::where('user_id', $user->id)->get()->each(function($userRole){
            $userRole->delete();
        });

        foreach($user_roles AS $user_role){
            UserRole::create(['role_id' => $user_role, 'user_id' => $user->id]);
        }

        return redirect('/user/create')->with('status','success')->with('context', $this->getContext());
    }

    public function showForm($id=null)
    {
        $roles = Role::all();
        $context = $this->getContext();

        if(!is_null($id)){
            $user = User::find($id);
            $param = compact('roles','context', 'user');
        } else {
            $param = compact('roles','context');
        }

        return view('admin.user.create', $param);
    }

    public function view($id = null)
    {
        if(is_null($id)){
            $users = User::with('userroles.role')->paginate(15);
            $view = 'admin.user.list';
            $viewParam = compact('users');
            return view($view, $viewParam);
        } else {
            return $this->showForm($id);
        }
    }

    public function edit($id)
    {
        $roles = Role::all();
        $user = User::with('userroles.role')->find($id);
        return view('admin.user.create', compact('roles','user'));
    }

    public function changePassword(ChangeUserPassword $request)
    {
        //redirect
        $user = User::find($request->get('user_id'));
        $user->update(['password' => Hash::make($request->get('password'))]);
        return redirect('user/change-password/'.$request->get('user_id'))->with('status','success')->with('context', 'change-password');
    }

    public function export($type)
    {
        $user = User::all();
        event(new ReportRequested($user));
    }
}
