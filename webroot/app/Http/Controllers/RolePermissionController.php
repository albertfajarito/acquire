<?php

namespace App\Http\Controllers;

use App\Library\Permission\Collection;
use App\Models\Model;
use App\Models\Role;
use App\Models\RoleModelPermission;
use Illuminate\Http\Request;

class RolePermissionController extends Controller
{
    public function view($role_id)
    {
        $models = Model::all();
        $permissions = RoleModelPermission::where('role_id', $role_id)->get();
        $role = Role::where('id', $role_id)->first();
        $permission_data = $this->buildPermissionData($models, $permissions);
        return view('admin.permission.rolepermission', compact(['models','permissions','permission_data', 'role']));
    }

    public function save(Request $request)
    {
        Model::all()->each(function($model) use($request){
            RoleModelPermission::updateOrCreate([
                    'role_id' => $request->role_id,
                    'model_id' => $model->id
                ],[
                    'permission' => $request->has(strtolower($model->name))? array_sum($request->{strtolower($model->name)}) : 0
                ]);
        });
        return redirect('/role/permission/'.$request->role_id)->with('status', 'success');
    }

    public function buildPermissionData($models, $permissions)
    {
        $permissionData = $models->map(function($model) use($permissions){

            $found = collect($permissions->toArray())->where('model_id', $model->id)->first();

            $permissionbit = 0;
            if(!is_null($found) && count($found) > 0){
                $permissionbit = $found['permission'];
            }
            $found = collect(['name' => strtolower($model->name),])->merge((new Collection( $permissionbit ))->build());
            return $found;
        });

        return $permissionData;
    }
}
