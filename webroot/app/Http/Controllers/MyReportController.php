<?php

namespace App\Http\Controllers;

use App\Models\ReportRequest;
use Illuminate\Http\Request;

class MyReportController extends Controller
{
    //
    public function show($user_id)
    {
        $reports = ReportRequest::where('user_id',$user_id)->paginate(15);
        return view('dashboard.reports', compact('reports'));
    }
}
