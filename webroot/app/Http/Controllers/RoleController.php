<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Traits\ContextualView;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    use ContextualView;

    /**
     * Show the application admin.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        return view('admin.user.metrics');
    }

    public function create(\App\Http\Requests\Role $request)
    {
        $role = Role::create($request->toArray());
        return back()->with([
                'status' => 'success'
            ])
            ->with([
                'context' => $this->getContext()
            ]);
    }

    public function update(\App\Http\Requests\UpdateRole $request)
    {
        $role = Role::findOrFail($request->get('role_id'));

        $role->update($request->toArray());

        return back()->with('status','success')->with('context',$this->getContext());
    }

    public function showForm($id=null)
    {
        $context = $this->getContext();

        if(!is_null($id)){
            $role = Role::find($id);
            $param = compact('context','role');
        } else {
            $param = compact('context');
        }

        return view('admin.permission.rolecreate', $param);
    }

    public function view()
    {
        $roles = Role::all();
        $context = $this->getContext();
        return view('admin.permission.rolelist', compact('roles', 'context'));
    }
}
