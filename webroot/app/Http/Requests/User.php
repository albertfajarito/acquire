<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class User extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name' => 'required|alpha|min:3',
            'second_name' => 'required|alpha|min:3',
            'email' => 'email|unique:users,email',
            'password' => 'required|min:8',
            'confirm_password' => 'same:password',
            'user_roles' => 'required'
        ];
    }
}
