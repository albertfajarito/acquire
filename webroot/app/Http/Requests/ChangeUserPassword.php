<?php

namespace App\Http\Requests;

use App\Rules\CurrentPasswordMatch;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;

class ChangeUserPassword extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'user_id' => 'required',
            'current_password' => ['required', new CurrentPasswordMatch],
            'password' => 'required|min:8',
            'confirm_password' => 'same:password'
        ];
    }
}
