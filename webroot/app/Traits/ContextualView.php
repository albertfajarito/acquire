<?php

namespace App\Traits;

Trait ContextualView
{
    protected function getContext()
    {
        $routeParts = collect(explode("/", \request()->path()));

        foreach (['view', 'create', 'edit', 'change-password'] AS $action) {
            if ($routeParts->contains($action)) {
                return $action;
            }
        }
    }
}
