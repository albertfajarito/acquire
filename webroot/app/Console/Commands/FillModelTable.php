<?php

namespace App\Console\Commands;

use App\Models\Model;
use Illuminate\Console\Command;

class FillModelTable extends Command
{
    protected $except = ['Model', 'RoleModelPermission'];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'acquire:fill-model-table';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $models = \File::files(base_path('app/Models/'));
        foreach($models AS $model){
            $class_name = $model->getFilenameWithoutExtension();
            if($class_name == "Model"){
                continue;
            }
            Model::firstOrCreate(['name' => $class_name],[
                'name' => $class_name,
                'class' => 'App\Models\\'.$class_name
            ]);
        }

    }
}
