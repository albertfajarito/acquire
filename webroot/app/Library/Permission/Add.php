<?php

namespace App\Library\Permission;


class Add extends PermissionAbstract implements PermissionContract
{
    const name = 'add';
}
