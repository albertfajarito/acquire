<?php
/**
 * Created by PhpStorm.
 * User: albertfajarito
 * Date: 9/23/19
 * Time: 6:28 AM
 */

namespace App\Library\Permission;


interface PermissionContract
{
    public function canDo();

    public function getName();
}
