<?php

namespace App\Library\Permission;


abstract class PermissionAbstract
{
    const bitValue = 0;

    public function __construct($permission)
    {
        $this->permission = $permission;
        $this->name = $this->setName();
    }

    protected function setName()
    {
        $class_ns =explode("\\", get_class($this));
        return strtolower(end($class_ns));
    }

    public function getName()
    {
        return $this->name;
    }

    public function canDo()
    {
        return (config('permission.'.$this->getName().'.bit',0) & $this->permission) > 0;
    }
}
