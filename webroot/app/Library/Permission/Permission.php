<?php

namespace App\Library\Permission;


use Illuminate\Support\Str;

class Permission
{
    public function __construct($permissionType, $permission)
    {
        $this->permissionType = Str::studly($permissionType);
        $this->permission = $permission;
    }

    public function build()
    {
        if(method_exists($this, 'build'.$this->permissionType)){
            $this->{'build'.$this->permissionType}($this->permission);
        } else {
            try{
                $reflection = new \Reflection($this->permissionType);
                $class = "App\Library\Permission\\".$this->permissionType;
                return new $class($this->permission);
            } catch (\Exception $e) {

            }
        }
    }
}
