<?php

namespace App\Library\Permission;


class Collection
{
    public function __construct($permissionbit)
    {
        $this->permissionbit = $permissionbit;
    }

    public function build()
    {
        $collection = collect([]);

        foreach(config('permission',[]) AS $name => $permission){
            $collection->put($name, (new Permission($name, $this->permissionbit))->build());
        }

        return $collection;
    }
}
