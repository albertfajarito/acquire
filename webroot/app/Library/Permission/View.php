<?php

namespace App\Library\Permission;


class View extends PermissionAbstract implements PermissionContract
{
    const name = 'view';
}
