<?php

namespace App\Library\Permission;


class Edit extends PermissionAbstract implements PermissionContract
{
    const name = 'edit';
}
