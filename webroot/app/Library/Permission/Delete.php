<?php

namespace App\Library\Permission;


class Delete extends PermissionAbstract implements PermissionContract
{
    const name = 'delete';
}
