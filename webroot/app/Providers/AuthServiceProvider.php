<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Log;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
        Gate::define('is-superuser', function($user){
            return $user->is_superuser;
        });

        Gate::define('edit-user', function($user){

            $result = false;

            $user->userroles->each(function($userrole) use(&$result){

                $userrole->role->rolemodelpermission->each(function($rolemodelpermission) use(&$result){

                    if($rolemodelpermission->model->name == 'User' && $rolemodelpermission->permission & config('permission.edit.bit') > 0){
                        $result = true;
                        return false;
                    }

                });

                if($result){
                    return false;
                }

            });

            return $result;
        });

        Gate::define('add-user', function($user){
            $result = false;

            $user->userroles->each(function($userrole) use(&$result){

                $userrole->role->rolemodelpermission->each(function($rolemodelpermission) use(&$result){

                    if($rolemodelpermission->model->name == 'User' && $rolemodelpermission->permission & config('permission.add.bit') > 0){
                        $result = true;
                        return false;
                    }

                });

                if($result){
                    return false;
                }

            });

            return $result;
        });

        Gate::define('delete-user', function($user){
            $result = false;

            $user->userroles->each(function($userrole) use(&$result){

                $userrole->role->rolemodelpermission->each(function($rolemodelpermission) use(&$result){

                    if($rolemodelpermission->model->name == 'User' && $rolemodelpermission->permission & config('permission.delete.bit') > 0){
                        $result = true;
                        return false;
                    }

                });

                if($result){
                    return false;
                }

            });

            return $result;
        });

        Gate::define('view-user', function($user){
            $result = false;

            $user->userroles->each(function($userrole) use(&$result){

                $userrole->role->rolemodelpermission->each(function($rolemodelpermission) use(&$result){

                    if($rolemodelpermission->model->name == 'User' && $rolemodelpermission->permission & config('permission.view.bit') > 0){
                        $result = true;
                        return false;
                    }

                });

                if($result){
                    return false;
                }

            });

            return $result;
        });

        Gate::define('is-admin', function($user){
            $result = false;
            $user->userroles->each(function($userrole) use(&$result){

                if($userrole->role->isAdmin){
                    $result = true;
                    return false;
                }
            });

            return true;
        });

        Gate::define('edit-role', function($user){

            $result = false;

            $user->userroles->each(function($userrole) use(&$result){

                $userrole->role->rolemodelpermission->each(function($rolemodelpermission) use(&$result){

                    if($rolemodelpermission->model->name == 'Role' && $rolemodelpermission->permission & config('permission.edit.bit') > 0){
                        $result = true;
                        return false;
                    }

                });

                if($result){
                    return false;
                }

            });

            return $result;
        });

        Gate::define('add-role', function($user){

            $result = false;

            $user->userroles->each(function($userrole) use(&$result){

                $userrole->role->rolemodelpermission->each(function($rolemodelpermission) use(&$result){

                    if($rolemodelpermission->model->name == 'Role' && $rolemodelpermission->permission & config('permission.add.bit') > 0){
                        $result = true;
                        return false;
                    }

                });

                if($result){
                    return false;
                }

            });

            return $result;
        });

        Gate::define('delete-role', function($user){
            $result = false;

            $user->userroles->each(function($userrole) use(&$result){

                $userrole->role->rolemodelpermission->each(function($rolemodelpermission) use(&$result){

                    if($rolemodelpermission->model->name == 'Role' && $rolemodelpermission->permission & config('permission.delete.bit') > 0){
                        $result = true;
                        return false;
                    }

                });

                if($result){
                    return false;
                }

            });

            return $result;
        });

        Gate::define('view-role', function($user){
            $result = false;

            $user->userroles->each(function($userrole) use(&$result){

                $userrole->role->rolemodelpermission->each(function($rolemodelpermission) use(&$result){

                    if($rolemodelpermission->model->name == 'Role' && $rolemodelpermission->permission & config('permission.view.bit') > 0){
                        $result = true;
                        return false;
                    }

                });

                if($result){
                    return false;
                }

            });

            return $result;
        });

        Gate::define('is-superuser', function($user){
            $result = false;
            $user->userroles->each(function($userrole) use(&$result){
                if($userrole->role->id == 1 && $userrole->role->permission & config('permission.view.bit') > 0 ){
                    $result = true;
                    return false;
                }
            });

            return $result;
        });

        Gate::before(function($user, $ability){
           if($user->is_superuser){
               return true;
           }
        });
    }
}
