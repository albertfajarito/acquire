<?php

namespace App\Events;

use App\ReportRequest\Models;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ReportRequested
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $reportRequest;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(ReportRequest $reportRequest)
    {
        //
        $this->reportRequest = $reportRequest;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
