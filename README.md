Acquire Coding Challenge
------------------------
CRUD System with RBAC, Reporting and API functionalities.

Requirements
------------------------
1. Vagrant
2. Virtualbox

Local Deployment Steps For Windows:
-----------------------
1. Save project locally
2. Open a cmd as administrator
3. In cmd go to folder where project was saved
4. Go inside the folder where 'bootstrap.sh' is located
5. Type and run: 'vagrant up'
6. After installation edit your hosts file and add: 10.11.12.55 acquire.local

Local Deployment Steps For Mac or Linux:
-----------------------
1. Save project locally
2. Open a terminal
3. In terminal go to folder where project was saved
4. Go inside the folder where 'bootstrap.sh' is located
5. Type and run: 'vagrant up'
6. After installation edit your hosts file and add: 10.11.12.55 acquire.local

Accessing App Locally 
-----------------------
url: http://acquire.local

**Login Credentials**
username: admin@acquire.com
password: password

@todo
-----------------------
* Implement reporting functionality
* Implement API functionality
* Implement RBAC to controller level